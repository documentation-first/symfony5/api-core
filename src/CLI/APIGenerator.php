<?php declare(strict_types=1);

namespace DocumentationFirst\ApiCoreBundle\CLI;

use DocumentationFirst\ApiCoreBundle\Configuration\ResourceManager;
use DocumentationFirst\ApiCoreBundle\Generator\Controller;
use DocumentationFirst\ApiCoreBundle\Parser\Swagger3;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\File;

class APIGenerator extends Command implements ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;
    /** @var Controller */
    private $controllerGenerator;
    /** @var ResourceManager */
    private $resourceManager;

    protected static $defaultName = 'dfd:api:generate';

    /**
     * APIGenerator constructor.
     *
     * @param Controller      $controllerGenerator
     * @param ResourceManager $resourceManager
     */
    public function __construct(Controller $controllerGenerator, ResourceManager $resourceManager)
    {
        parent::__construct();
        $this->controllerGenerator = $controllerGenerator;
        $this->resourceManager = $resourceManager;
    }

    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Sample command, hello')
            ->addOption('dump')
            ->setHelp('This command is a sample command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $apiConfig = $this->container->getParameter('api');
        $generationConfig = $this->container->getParameter('generation');

        echo "Type: ".$apiConfig['type']."\n";

        $schemaPath = $this->container->getParameter('kernel.project_dir').'/'.$apiConfig['schema'];
        echo "Schema: ".$schemaPath."\n";

        $swg = new Swagger3(new File($schemaPath));
        $swg->hasDetail('users');

        // if ($input->hasOption('dump')) {
        //     var_dump($this->container->getParameter('resources'));
        //     return Command::SUCCESS;
        // }

        $controllerGenerator = $this->container->get(Controller::class);
        $resourceManager = $this->container->get(ResourceManager::class);

        foreach ($resourceManager->getResources() as $resource) {
            $controllerGenerator->generate(
                $resource
            );
        }

        return Command::SUCCESS;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
