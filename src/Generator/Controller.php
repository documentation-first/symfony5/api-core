<?php

declare(strict_types=1);

namespace DocumentationFirst\ApiCoreBundle\Generator;

use DocumentationFirst\ApiCoreBundle\CLI\APIGenerator;
use DocumentationFirst\ApiCoreBundle\Configuration\Resource;

class Controller
{
    /** @var Twig */
    private $twig;

    /** @var string */
    private $namespace;

    /** @var string */
    private $path;

    /**
     * Controller constructor.
     *
     * @param Twig  $twig
     * @param array $generationConfig
     */
    public function __construct(Twig $twig, array $generationConfig)
    {
        $this->twig = $twig;
        $this->namespace = $generationConfig['controllers']['namespace'];
        $this->path = $generationConfig['controllers']['path'];
    }

    public function generate(Resource $resource): void
    {
        $template = $this->twig->load('controller.php.twig');
        $controllerName = "{$resource->getShortName()}Controller";
        $path = $this->path."/$controllerName.php";
        file_put_contents($path, $template->render([
            'namespace' => $this->namespace,
            'routePrefix' => 'dfd_',
            'controllerName' => $controllerName,
            'resource' => $resource,
        ]));
        echo "Generated $path\n";
    }
}
