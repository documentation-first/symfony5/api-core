<?php

declare(strict_types=1);

namespace DocumentationFirst\ApiCoreBundle\Parser;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Yaml\Yaml;

class Swagger3
{
    /** @var File */
    private $schemaFile;

    /** @var array[] */
    private $projectRules;

    /**
     * Swagger3 constructor.
     *
     * @param File $schemaFile
     * @param array[]     $projectRules
     */
    public function __construct(File $schemaFile, array $projectRules = [])
    {
        $this->schemaFile = $schemaFile;
        $this->projectRules = $projectRules;
    }

    public function hasDetail(string $resource): bool
    {
        $swgSchema = Yaml::parseFile($this->schemaFile->getRealPath());

        // var_dump($swgSchema);
        foreach ($swgSchema['paths'] as $path => $pathSetting) {
            echo $path."\n";
        }

        return false;
    }
}
