<?php

declare(strict_types=1);

namespace DocumentationFirst\ApiCoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class DocumentationFirstApiCoreBundle extends Bundle
{
    
}
