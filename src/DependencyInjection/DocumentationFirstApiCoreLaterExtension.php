<?php

declare(strict_types=1);

namespace DocumentationFirst\ApiCoreBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

class DocumentationFirstApiCoreLaterExtension extends ConfigurableExtension
{
    protected function loadInternal(array $mergedConfig, ContainerBuilder $container)
    {
        $definition = $container->getDefinition('api');
        $definition->replaceArgument('type', $mergedConfig['api']['type']);
        $definition->replaceArgument('location', $mergedConfig['api']['location']);
    }
}
