<?php

declare(strict_types=1);

namespace DocumentationFirst\ApiCoreBundle\Configuration;

class ResourceDetail
{
    /** @var string */
    private $resource;

    /** @var string */
    private $name;

    /** @var string */
    private $url;

    /**
     * ResourceDetail constructor.
     *
     * @param string $resource
     * @param string $name
     * @param string $url
     */
    public function __construct(string $resource, string $name, string $url)
    {
        $this->resource = $resource;
        $this->name = $name;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getResource(): string
    {
        return $this->resource;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

}
