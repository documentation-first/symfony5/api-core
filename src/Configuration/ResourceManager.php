<?php

declare(strict_types=1);

namespace DocumentationFirst\ApiCoreBundle\Configuration;

class ResourceManager
{
    /** @var string[][] */
    private $resourceSets;

    /**
     * ResourceSettingManager constructor.
     *
     * @param string[][] $settings
     */
    public function __construct(array $settings)
    {
        $this->resourceSets = $settings;
    }

    /**
     * @return Resource[]
     */
    public function getResources(): iterable
    {
        foreach ($this->resourceSets as $resourceId => $config) {
            yield new Resource($resourceId, $config['entityClass'], $config);
        }
    }

    /**
     * @param string $resourceId
     *
     * @return ResourceDetail[]
     */
    public function getDetails(string $resourceId): iterable
    {
        $resourceConfig = $this->resourceSets[$resourceId];

        if (!isset($resourceConfig['detail'])) {
            return [];
        }

        if (!is_array($resourceConfig['detail'])) {
            $details['detail'] = $resourceConfig['detail'];
        } else {
            $details = $resourceConfig['detail'];
        }


        foreach ($details as $detailName => $customDetailSetting) {
            $detailSettings = array_merge($this->defaults['details'], $customDetailSetting);

            $prefix = $this->getSingularUrlPrefix($resourceId);
            $path = $detailSettings['path'];
            $path = str_replace('{prefix}', $prefix, $path);
            $path = str_replace('{detailName}', $detailName, $path);
            yield new ResourceDetail(
                $resourceId,
                $detailName,
                $path
            );
        }
    }

    private function getSingularUrlPrefix(string $resource): string
    {
        $resourceConfig = $this->resourceSets[$resource];
        if (isset($resourceConfig['urlPrefix'])
            && !is_array($resourceConfig['urlPrefix'])
            && is_string($resourceConfig['urlPrefix'])) {
            return $resourceConfig['urlPrefix'];
        }
        return $resourceConfig['urlPrefix']['singular'];
    }

}
