<?php

declare(strict_types=1);

namespace DocumentationFirst\ApiCoreBundle\Configuration;

class Resource
{
    /** @var string */
    private $id;
    /** @var string */
    private $shortName;
    /** @var string */
    private $className;
    /** @var string[][] */
    private $config;

    /**
     * Resource constructor.
     *
     * @param string $id
     * @param string $className
     * @param array  $config
     */
    public function __construct(string $id, string $className, array $config)
    {
        $reflectionClass = new \ReflectionClass($className);
        $this->shortName = $reflectionClass->getShortName();
        $this->className = $className;
        $this->id = $id;
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @return string[][]
     */
    public function getConfig(): array
    {
        return $this->config;
    }

}
