<?php

declare(strict_types=1);

namespace DocumentationFirst\ApiCoreBundle\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ResourcesFillerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        $resourcesSettings = $container->getParameter('resources');
        $generationDefaults = $container->getParameter('generation');

        foreach ($resourcesSettings as $resourceId => $resourceSettings) {
            $this->expandUrlPrefix($resourceSettings);
            $this->expandPath('details', 'detail', $resourceSettings, $generationDefaults);
            $this->expandPath('lists', 'list', $resourceSettings, $generationDefaults);
            $this->expandPath('updates', 'update', $resourceSettings, $generationDefaults);
            $this->expandPath('replaces', 'replace', $resourceSettings, $generationDefaults);
            $this->expandPath('deletes', 'delete', $resourceSettings, $generationDefaults);
            $resourcesSettings[$resourceId] = $resourceSettings;
        }
        $container->setParameter('resources', $resourcesSettings);
    }

    private function completeResource(string $resourceId, array $resourceSettings): array
    {
        return $resourceSettings;
    }

    private function expandPath(string $pathType, string $singularType, array &$resourceSettings, array $generationDefaults)
    {
        if (array_key_exists($singularType, $resourceSettings)) {
            $resourceSettings[$pathType][$singularType] = $resourceSettings[$singularType];
            unset($resourceSettings[$singularType]);
        }

        if (!array_key_exists($pathType, $resourceSettings)) {
            return;
        }

        foreach ($resourceSettings[$pathType] as $pathName => &$pathSettings) {
            $resourceSettings[$pathType][$pathName] = array_merge($generationDefaults[$pathType] ?? [], $pathSettings ?? []);

            $path = $pathSettings['path'];
            $path = str_replace('{prefix}', $resourceSettings['urlPrefix']['plural'], $path);
            $path = str_replace('{pathName}', $pathName, $path);
            $pathSettings['path'] = $path;
        }
    }

    private function expandUrlPrefix(array &$resourceSettings)
    {
        if (!isset($resourceSettings['urlPrefix'])) {
            $resourceSettings['urlPrefix'] = '/';
        }

        if (!is_array($resourceSettings['urlPrefix'])) {
            $resourceSettings['urlPrefix'] = [
                'singular' => $resourceSettings['urlPrefix'],
                'plural' => $resourceSettings['urlPrefix'],
            ];
        }
    }

}
